json.extract! fight, :id, :schedule, :challenger_id, :challenged_id, :created_at, :updated_at
json.url fight_url(fight, format: :json)
