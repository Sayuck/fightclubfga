class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  validates :username,:firstname, :lastname, presence: true
  validates :username, length: { :within=> 3..10}
  validates :firstname, :lastname, length: {:within => 3..10}

  has_many :fights
end
