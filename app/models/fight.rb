class Fight < ApplicationRecord
  acts_as_votable
  belongs_to :challenger,
             :class_name => 'User',
             :foreign_key  => "challenger_id"
  belongs_to :challenged,
             :class_name => "User",
             :foreign_key  => "challenged_id"

  has_many :comments
  
end
