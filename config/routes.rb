Rails.application.routes.draw do
  resources :comments
  resources :fights do
    member do
      put "like",    to: "fights#upvote"
      put "dislike", to: "fights#downvote"
    end
    resources :comments
  end
  get 'home/index'
  devise_for :users, :controllers => {registrations: 'registrations'}
  root to: 'home#index'
  
  #root to: "home#index"

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
