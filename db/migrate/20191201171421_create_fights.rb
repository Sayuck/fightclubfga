class CreateFights < ActiveRecord::Migration[6.0]
  def change
    create_table :fights do |t|
      t.datetime :schedule
      t.references :challenger
      t.references :challenged

      
    end
    add_foreign_key :fights, :users, column: :challenger_id, primary_key: :id
    add_foreign_key :fights, :users, column: :challenged_id, primary_key: :id  
  end
end
