# FightClubFGA

Fight Club é uma aplicação web para marcar lutas para desestressar.

## Pré-Requisitos

* Ruby 2.6.5
* Rails 6.0.1
* [Yarn](https://yarnpkg.com/en/docs/install)

## Execução 
Execute os seguintes comandos:
##### 1. Clone o repositório
 ```
 git clone https://gitlab.com/Sayuck/fightclubfga.git
 ```
 
##### 2. Entre no diretório do projeto
 ```
 cd fightclubfga
 ```
##### 3. Instale as dependências
```
yarn install
bundle install
 ```
##### 4. Crie o banco de dados
```
rake db:migrate
 ```
##### 5. Inicialize o servidor Rails
```
rails s
 ```

